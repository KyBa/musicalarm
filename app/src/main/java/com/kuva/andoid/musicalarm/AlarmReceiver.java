package com.kuva.andoid.musicalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import java.io.File;

/**
 * Created by KuVa on 13.09.2015.
 */
public class AlarmReceiver extends BroadcastReceiver
{
    private static final String FILE_NAME = "Music.mp3";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String filePath = (Environment.getExternalStorageDirectory()     + File.separator + FILE_NAME);
        PlayMusicService.startService(context, filePath);
    }
}
