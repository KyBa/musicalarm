package com.kuva.andoid.musicalarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by KuVa on 11.09.2015.
 */
public class DownloadService extends Service
{

    private static final String URL_KEY = "URL_KEY";
    private static final String FILE_PATH_KEY = "FILE_PATH_KEY";
    private static final String LOG = "DownloadService";
    private static final int ID = 2;

    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;

    public static void startService(Context context, String url, String filePath)
    {
        Intent intent = new Intent(context, DownloadService.class);
        intent.putExtra(URL_KEY, url);
        intent.putExtra(FILE_PATH_KEY, filePath);
        context.startService(intent);
    }

    @Override
    public void onCreate()
    {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        builder =  new NotificationCompat.Builder(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                String url = null;
                String filePath = null;

                setForegroundNotification();

                if (intent!= null)
                {
                    url = intent.getStringExtra(URL_KEY);
                    filePath = intent.getStringExtra(FILE_PATH_KEY);
                }
                Log.d(LOG, "Download url = " + url);

                downLoadFile(url, filePath);
                notificationManager.cancel(ID);

                stopSelf();
            }
        }).start();


        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    private void downLoadFile(String urlString, String filePath)
    {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;


        try
        {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();

            int responseCode = connection.getResponseCode();
            if (responseCode == 200)
            {
                inputStream = connection.getInputStream();
                long lenghtOfFile = connection.getContentLength();

                outputStream = new FileOutputStream(filePath);


                byte [] buffer = new byte[6000];
                int count;
                int total = 0;
                int progress = 0;
                int nProgress = 0;
                while ((count = inputStream.read(buffer)) != -1)
                {
                    total += count;
                    progress = (int) ((total * 100) / lenghtOfFile);
                    if (nProgress != progress)
                    {
                        updateNotification(progress);
                        nProgress = progress;
                    }
                    Log.d(LOG, (progress + " %"));

                    outputStream.write(buffer, 0, count);
                }

                Log.d(LOG, "File Downloaded");

            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (connection != null)
                connection.disconnect();

            try
            {

                if (inputStream != null)
                    inputStream.close();

                if (outputStream != null)
                    outputStream.close();

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void setForegroundNotification()
    {
        builder.setSmallIcon(R.drawable.download_icon)
                        .setContentTitle("Downloading file")
                        .setContentText("0 %");

        notificationManager.notify(ID, builder.build());
    }

    private void updateNotification(int progress)
    {
        builder.setContentText(String.valueOf(progress) + " %");

        notificationManager.notify(ID, builder.build());
    }



}
