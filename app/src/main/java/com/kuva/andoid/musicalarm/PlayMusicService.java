package com.kuva.andoid.musicalarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;


import java.io.IOException;

/**
 * Created by KuVa on 13.09.2015.
 */
public class PlayMusicService extends Service
{

    private static final String FILE_PATH_KEY = "FILE_PATH_KEY";
    private static final int ID = 1;

    MediaPlayer mediaPlayer;

    public static void startService(Context context, String filePath)
    {
        Intent intent = new Intent(context, PlayMusicService.class);
        intent.putExtra(FILE_PATH_KEY, filePath);
        context.startService(intent);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        final String filePath  = intent.getStringExtra(FILE_PATH_KEY);
        if (mediaPlayer != null) {
        return START_NOT_STICKY;
        }

        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                mediaPlayer = new MediaPlayer();

                try
                {
                    mediaPlayer.setDataSource(filePath);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    setForegroundNotification();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }

                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        stopSelf();

                    }
                });

            }
        }).start();

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy()
    {
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
        stopForeground(true);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    private void setForegroundNotification()
    {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.play_icon)
                        .setContentTitle("Playing music")
                        .setContentText("BITCH");

        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
        builder.setContentIntent(resultPendingIntent);
        startForeground(ID, builder.build());
    }
}
