package com.kuva.andoid.musicalarm;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.File;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity
{

    private static final String FILE_NAME = "Music.mp3";
    private static String FILE_PATH;
    private static final String URL = "http://incompetech.com/music/royalty-free/mp3-royaltyfree/Meanwhile%20in%20Bavaria.mp3";

    private TimePicker timePicker;
    private Button setAlarmButton;
    private Button playButton;
    private Button stopButton;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            FILE_PATH = (Environment.getExternalStorageDirectory() + File.separator + FILE_NAME);
        else
        {
            FILE_PATH = FILE_NAME;      // another path here !!
        }


        timePicker = (TimePicker) findViewById(R.id.time_picker);
        timePicker.setIs24HourView(true);

        setAlarmButton = (Button) findViewById(R.id.do_it_button);
        setAlarmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                DownloadService.startService(MainActivity.this, URL , FILE_PATH);
                int hour = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();

                Intent startIntent = new Intent(MainActivity.this, AlarmReceiver.class);
                PendingIntent startPIntent = PendingIntent.getBroadcast(MainActivity.this, 0, startIntent, 0);
                AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY  , hour);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.SECOND, 0);

                alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), startPIntent);
            }
        });

        playButton = (Button) findViewById(R.id.play_button);
        playButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               PlayMusicService.startService(MainActivity.this, FILE_PATH);
            }
        });

        stopButton = (Button) findViewById(R.id.stop_button);
        stopButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, PlayMusicService.class);
                stopService(intent);

               // Toast.makeText(MainActivity.this, String.valueOf(isStopped), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
